<?php

namespace Drupal\gtranslate_notranslate\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\locale\StringDatabaseStorage;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber that looks through the response data and makes data no translatable.
 */
class NoTranslateSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Set the context that should be search for.
   */
  protected array $context = [];

  /**
   * Creates a new NoTranslateSubscriber Object.
   */
  public function __construct(protected LanguageManagerInterface $languageManager, protected StringDatabaseStorage $localeStorage, protected AdminContext $adminContext, protected ConfigFactoryInterface $configFactory) {
    $this->context = $this->configFactory->get('gtranslate_notranslate.settings')->get('context') ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::RESPONSE][] = ['response', -10000];
    return $events;
  }

  /**
   * Alter the HTML before sent to the browser.
   */
  public function response(ResponseEvent $event) {
    $response = $event->getResponse();

    // Skip if current Language is English.
    if (
      $this->languageManager->getCurrentLanguage()->getId() === 'en' ||
      $this->adminContext->isAdminRoute()
    ) {
      return;
    }

    // Make sure that the following render classes are the only ones that
    // are allowed to alter data.
    $allowed_response_classes = [
      'Drupal\big_pipe\Render\BigPipeResponse',
      'Drupal\Core\Render\HtmlResponse',
    ];
    if (in_array(get_class($response), $allowed_response_classes)) {
      $content = $this->alterContent($response->getContent());
      $response->setContent($content);
    }
  }

  /**
   * Alter the content when searching for the provided items.
   *
   * @param string $html
   *   Content to search and modify.
   *
   * @return string
   *   Modified content.
   */
  protected function alterContent(string $html): string {
    // Load the HTML into the Crawler
    $crawler = new Crawler($html);

    // Get the source strings to be replaced
    $sourceStrings = $this->getStrings();
    $replacements = [];

    // Define a pattern to match the source strings, considering spaces and HTML encoded characters
    foreach ($sourceStrings as $sourceString) {
      $findString = preg_quote($sourceString->getString(), '/');
      $pattern = '/\b' . str_replace(' ', '(?:\s|&nbsp;|&#[0-9]+;|&[a-z]+;)*', $findString) . '\b/i';

      // Iterate through all text nodes in the body
      $crawler->filter('body')->each(function (Crawler $node) use ($pattern, &$replacements, $sourceString) {
        $node->filterXPath('//text()[not(ancestor::svg) and not(ancestor::script) and not(ancestor::style)]')->each(function (Crawler $textNode) use ($pattern, &$replacements, $sourceString) {
          $content = $textNode->text();
          if (preg_match_all($pattern, $content, $matches, PREG_OFFSET_CAPTURE)) {
            foreach ($matches[0] as $match) {
              $fullMatch = $match[0];
              $replacementId = '___' . md5($fullMatch) . '___';
              $replacements[$replacementId] = $this->t($sourceString->getString())->__toString();
              $newContent = str_replace($fullMatch, $replacementId, $content);
              $textNode->getNode(0)->nodeValue = $newContent;
            }
          }
        });
      });
    }

    $replacements = array_map(function ($replacement) {
      return '<span class="notranslate" notranslate>' . $replacement . '</span>';
    }, $replacements);

    // Convert the modified DOM back to HTML
    return str_ireplace(array_keys($replacements), array_values($replacements), $crawler->html());
  }

  /**
   * Return a list of all the strings from a certain context.
   *
   * @return \Drupal\locale\StringInterface[]
   *   Array of string items.
   */
  protected function getStrings(): array {
    $allStrings = [];

    foreach ($this->context as $context) {
      $allStrings = array_merge($allStrings, $this->getStringsByContext($context));
    }

    return $this->sortStrings($allStrings);
  }

  /**
   * Sort the strings based on longest first.
   *
   * @param \Drupal\locale\StringInterface[] $strings
   *   Array of strings items.
   *
   * @return \Drupal\locale\StringInterface[]
   *   Array of string items.
   */
  protected function sortStrings(array $strings): array {
    // Sort dictionary by length in descending order to handle collisions
    usort($strings, function($a, $b) {
      return strlen($b->getString()) - strlen($a->getString());
    });
    return $strings;
  }

  /**
   * Return a list of strings for the provided context.
   *
   * @param string $context
   *   Context to search for.
   *
   * @return \Drupal\locale\StringInterface[]
   *   Array of string items.
   */
  protected function getStringsByContext(string $context): array {
    return $this->localeStorage->getStrings([
      'context' => $context,
    ]);
  }

}

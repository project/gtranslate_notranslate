<?php

namespace Drupal\gtranslate_notranslate\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Settings Form.
 */
class GTranslateNoTranslateSettingsForm extends ConfigFormBase {

  /**
   * Module Handler Service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Returns the module handler service.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected function moduleHandler(): ModuleHandlerInterface {
    if (!isset($this->moduleHandler)) {
      $this->moduleHandler = \Drupal::moduleHandler();
    }
    return $this->moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gtranslate_notranslate.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gtranslate_notranslate_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gtranslate_notranslate.settings');

    $form['description'] = [
      '#type' => 'container',
      'text' => [
        '#markup' => $this->t('Configure the Settings used for the GTranslate NoTranslate module.'),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ],
      '#weight' => -20,
    ];

    if (!$this->moduleHandler()->moduleExists('string_translation_ui')) {
      $text = $this->t('To help manage the strings better through the UI the following module can help with using the interface @string_translation_ui.', [
        '@string_translation_ui' => Link::fromTextAndUrl('String Translation UI', Url::fromUri('https://www.drupal.org/project/string_translation_ui'))->toString(),
      ]);
    }
    else {
      $text = $this->t('The String Translation UI module has been installed. To add strings and contexts go to the @string_translation_ui.', [
        '@string_translation_ui' => Link::fromTextAndUrl('Add Strings UI Form', Url::fromRoute('string_translation_ui.add_strings'))->toString(),
      ]);
    }

    $form['configure'] = [
      '#type' => 'container',
      'text' => [
        '#markup' => $text,
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ],
      '#weight' => -20,
    ];

    $form['context'] = [
      '#type' => 'checkboxes',
      '#required' => FALSE,
      '#title' => $this->t('Contexts'),
      '#description' => $this->t('Select the contexts of the strings that should be searched for and not translated using G-Translate.'),
      '#default_value' => $config->get('context') ?? [],
      '#options' => $this->getContexts(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Return a list of all the contexts currently.
   *
   * @return string[]
   *   Array of strings for contexts.
   *
   * @throws \Exception
   *   Exception is thrown if issues connecting to the database.
   */
  protected function getContexts() {
    $query = \Drupal::database()->select('locales_source', 'ls');
    $query->fields('ls', ['context', 'context']);
    $query->distinct();
    $query->orderBy('ls.context');
    return $query->execute()->fetchAllKeyed();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('gtranslate_notranslate.settings')
      ->set('context', array_keys(array_filter($form_state->getValue('context'))))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

GTranslate NoTranslate

The following module will allow for the site administrator to select the string contexts that should be searched for
on the page and replaced with a notranslate class.

Inspiration for this came because there are just some words and phrases that either shouldn't be translated or they
are meant to be translated in a particular way.

You might use the following module for the following use cases:

Product names: For example, "Product XYZ" must translate to "Product XYZ".

Ambiguous words: For example, the word "bat" can mean a piece of sports equipment or an animal. If you know that
you are translating words about sports, you might want to use the following to feed the GTranslate service the sports
translation of "bat", not the translation for the animal.

Borrowed words: For example, "bouillabaisse" in French translates to "bouillabaisse" in English. English borrowed the
word "bouillabaisse" from French in the 19th century. An English speaker lacking French cultural context might not know
that bouillabaisse is a fish stew dish. The following can override a translation so that "bouillabaisse" in French
translates to "fish stew" in English.
